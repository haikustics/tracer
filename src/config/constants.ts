import { Vector3 } from '@models'
import { vectorize } from '@helpers/math'

export const ORIGIN: Vector3 = vectorize(0, 0, 0)
export const X_AXIS: Vector3 = vectorize(1, 0, 0)
export const Y_AXIS: Vector3 = vectorize(0, 1, 0)
export const Z_AXIS: Vector3 = vectorize(0, 0, 1)

export const FUDGE_FACTOR = 0.00001

