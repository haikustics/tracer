// Euler-Poincaré lookup enum
export enum MeshType {
  TripleTorus = -4,
  DoubleTorus = -2,
  Torus = 0,
  Open = 1,
  Enclosed = 2,
}

// surface material type
export enum MaterialType {
  Ceiling,
  Wall,
  Floor,
  Fenestration,
  Mechanical,
  Environmental,
  Acoustical,
  People,
  Custom,
  Unknown,
}

