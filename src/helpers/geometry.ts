import { Triangle3, Polygon3, Vector3 } from '@models'
import { Type2D } from '@models/conditionals'
import { X_AXIS, Y_AXIS, Z_AXIS } from '@constants'
import { dot, cross, subtract, negate, unitVector, vectorize } from '@helpers/math'

import earcut from 'earcut'

// get normal of triangle points ABC
const pointNormal = (...vecs: Vector3[]): Vector3 => {
  return unitVector(cross(
    subtract(vecs[1], vecs[2]),
    subtract(vecs[1], vecs[0])
  ))
}

// get normal of polygon using first three vertices
export const planeNormal = (polygon: Polygon3): Vector3 => {
  return pointNormal(...polygon.vertices.slice(0, 3))
}

// transpose a 3D polygon / triangle to 2D
// NOTE: this is required because of earcut limitations
export const convert3Dto2D = <T extends Triangle3|Polygon3> (
  polygon3: T
): Type2D<T> => {
  const origin = polygon3.vertices[0]
  const normal = planeNormal(polygon3)
  let xAxis: Vector3, yAxis: Vector3
  // TODO: improve this logic
  // right now this should only work for simple 3D rects
  if (normal.x.equals(X_AXIS.x.neg())) {
    xAxis = Z_AXIS
    yAxis = Y_AXIS
  } else if (normal.x.equals(X_AXIS.x)) {
    xAxis = negate(Z_AXIS)
    yAxis = Y_AXIS
  } else {
    yAxis = unitVector(cross(normal, X_AXIS))
    xAxis = unitVector(cross(yAxis, normal))
  }
  return {
    vertices: polygon3.vertices.map(vertex => {
      const diff: Vector3 = subtract(vertex, origin)
      return {
        x: dot(diff, xAxis),
        y: dot(diff, yAxis)
      }
    })
  } as Type2D<T>
}

// cut a 3D polygon into triangles
export const triangulate = (polygon: Polygon3): Triangle3[] => {
  // format 2D polygon to earcut input array [ax, ay, bx, by, ...]
  const earcutArray = convert3Dto2D(polygon).vertices.flatMap(
    v => [v.x.valueOf(), v.y.valueOf()]
  )
  // invoke earcut to get array of triangle indices [xa,xb,xc, ya,yb,yc, ...]
  const indices: number[] = earcut(earcutArray)

  const triangles: Triangle3[] = []
  while (indices.length) {
    triangles.push({
      vertices: indices.splice(0, 3).map(i => polygon.vertices[i])
    } as Triangle3)
  }
  return triangles
}

// test if point is inside line on plane
const insideLine = (
  point: Vector3, v1: Vector3, v2: Vector3, normal: Vector3
): boolean => {
  return dot(normal, cross(subtract(v2, v1), subtract(point, v1))).valueOf() >= 0
}

// test if point is inside triangle
export const insideTriangle = (
  point: Vector3, normal: Vector3, triangle: Triangle3
): boolean => {
  return triangle.vertices.every((v, i) => {
    return insideLine(point, v, triangle.vertices[(i + 1) % 3], normal)
  })
}

// test if array of points is coplanar
export const testCoplanar = (points: Vector3[]): boolean => {
  // three or fewer points are always coplanar
  if (points.length < 4) return true
  // point B lies on plane through point A with normal P if dot(A - B, P) is 0
  const normal = pointNormal(...points.slice(0, 3))
  return points.slice(3).every(p => dot(subtract(points[0], p), normal).equals(0))
}

export const fibonacciSphere = (
  samples: number, randomize = true
): Vector3[] => {
  const rnd = randomize ? Math.random() * samples : 1
  const offset = 2 / samples
  const increment = Math.PI * (3 - Math.sqrt(5))

  return Array(samples).fill({}).map((_, i) => {
    const y = ((i * offset) - 1) + (offset / 2)

    const r = Math.sqrt(1 - Math.pow(y, 2))
    const phi = ((i + rnd) % samples) * increment

    const x = Math.cos(phi) * r
    const z = Math.sin(phi) * r

    return vectorize(x, y, z)
  })
}
