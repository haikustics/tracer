import Fraction from 'fraction.js'
import { Vector, Vector2, Vector3 } from '@models/vector'

// iterate over vector components (x, y, z?)
const mapVector = <T extends Vector> (
  vector: T,
  mapFunction: (_key: string, _value: Fraction) => Fraction
): T => {
  return Object.entries(vector).reduce(
    (o, [key, val]: [string, Fraction]) => {
      return {...o, [key]: mapFunction(key, val)}
    }, {} as T
  )
}

// vector magnitude - sqrt( x^2 + y^2 + z?^2 )
const mag = <T extends Vector> (vector: T): number => {
  return Math.sqrt(
    (Object.values(vector) as Fraction[]).reduce(
      (acc, val) => acc.add(val.pow(2)),
      new Fraction(0)
    ).valueOf()
  )
}

// create a new vector from supplied numbers
// TODO: make a class for this?
export const vectorize = <T extends number | undefined> (
  x: number,
  y: number,
  z?: T,
): T extends number ? Vector3: Vector2 => {
  const v = {
    x: new Fraction(x),
    y: new Fraction(y),
    z: typeof z === 'number' ? new Fraction(z) : undefined,
  }
  return v
}

// dot product
// resultant scalar is length of v1's "shadow" over v2
export const dot = <T extends Vector> (v1: T, v2: T): Fraction => {
  return Object.entries(v1).reduce((o, [key, val]: [string, Fraction]) => {
    return o.add(val.mul(v2[key] as Fraction))
  }, new Fraction(0))
}

// cross product
// resultant vector is perpendicular to both v1 and v2
export const cross = (v1: Vector3, v2: Vector3): Vector3 => {
  return {
    x: v1.y.mul(v2.z).sub(v1.z.mul(v2.y)),
    y: v1.z.mul(v2.x).sub(v1.x.mul(v2.z)),
    z: v1.x.mul(v2.y).sub(v1.y.mul(v2.x)),
  }
}

// unit vector
// maintain direction, set magnitude to 1
export const unitVector = <T extends Vector> (v: T): T => {
  if (mag(v) === 0) return v
  return mapVector(v, (_key,val) => val.div(mag(v)))
}

// negate vector components
export const negate = <T extends Vector> (v: T): T => {
  return mapVector(v, (_key,val) => val.neg())
}

// add two vectors by component
export const add = <T extends Vector> (v1: T, v2: T): T => {
  return mapVector(v1, (key,val) => val.add(v2[key] as Fraction))
}

// subtract two vectors by component
export const subtract = <T extends Vector> (v1: T, v2: T): T => {
  return mapVector(v1, (key,val) => val.sub(v2[key] as Fraction))
}

// multiply vector components by scalar
export const multiply = <T extends Vector> (v: T, scalar: Fraction): T => {
  return mapVector(v, (_key,val) => val.mul(scalar))
}

// divide vector components by scalar
export const divide = <T extends Vector> (v: T, scalar: Fraction): T => {
  return mapVector(v, (_key,val) => val.div(scalar))
}

// reflect vector 1 over vector 2
export const reflect = <T extends Vector> (v1: T, v2: T): T => {
  return subtract(v1, multiply(v2, dot(v1, v2).mul(2)))
}
