import { Ray, Room, Source } from '@models'
import { roomReflection } from '@helpers/ray'

export const reflectOrder = (ray: Ray, room: Room, order = 1): Ray => {
  const rtnRay: Ray = roomReflection(ray, room)
  if (order > 1) return reflectOrder(rtnRay, room, order - 1)
  return rtnRay
}

export const reflectSource = (source: Source, room: Room, order = 1): Ray[] => {
  return source.directions.map(d => {
    const ray = { origin: source.point, direction: d }
    return reflectOrder(ray, room, order)
  })
}

