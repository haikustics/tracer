import { FUDGE_FACTOR } from '@constants'
import {
  Triangle3, Ray, Room, Polygon3, Vector3, Plane, ReflectionPoint
} from '@models'
import { planeNormal, triangulate, insideTriangle } from './geometry'
import { dot, add, subtract, multiply, reflect } from './math'

// get intersection of ray and plane, if exists
const planeIntersection = (ray: Ray, plane: Plane): Vector3 => {

  // ray intersects plane if Ax + By + Cz + D = 0
  // D = distance from origin to plane parallel to plane's normal
  // (A,B,C) = components of plane normal
  // (x,y,z) = coordinates of any point on the plane
  const normalDotDirection = dot(plane.normal, ray.direction)

  // if ray is acute/perpendicular to normal, assume no intersection
  if (normalDotDirection.valueOf() > -FUDGE_FACTOR) return

  // ray parametric equation: P = O + tR
  // t = (P - O) / R
  // t = distance from origin O in direction R to intersection point P
  const t = dot(
    subtract(plane.vertices[0], ray.origin),
    plane.normal,
  ).div(normalDotDirection)

  //console.log(plane.vertices, plane.normal, normalDotDirection.valueOf(), t.valueOf())

  // if t is negative, plane is behind ray origin
  if (t.valueOf() < 0) return

  // compute plane intersection point (P)
  return add(ray.origin, multiply(ray.direction, t))

}

// triangles
const interactTriangle = (ray: Ray, triangle: Triangle3): ReflectionPoint => {
  const normal = planeNormal(triangle)
  const point = planeIntersection(
    ray,
    {
      normal: normal,
      vertices: triangle.vertices
    }
  )
  if (!point || !insideTriangle(point, normal, triangle)) return
  return { normal: normal, point: point }
}

const triangleIntersection = (ray: Ray, triangle: Triangle3): Vector3 => {
  const reflectionPoint = interactTriangle(ray, triangle)
  if (reflectionPoint)
    return reflectionPoint.point
}
const triangleReflection = (ray: Ray, triangle: Triangle3): Ray => {
  const reflectionPoint = interactTriangle(ray, triangle)
  if (reflectionPoint) return {
    origin: reflectionPoint.point,
    direction: reflect(ray.direction, reflectionPoint.normal)
  }
}

// polygons
const interactPolys = <T extends Polygon3|Triangle3, U extends Ray|Vector3> (
  ray: Ray, array: T[], f: (Ray, T) => U | undefined
): U => {
  for (const e of array) {
    const interaction = f(ray, e)
    if (interaction) return interaction
  }
}

const polygonIntersection = (ray: Ray, polygon: Polygon3): Vector3 => {
  return interactPolys(ray, triangulate(polygon), triangleIntersection)
}
const polygonReflection = (ray: Ray, polygon: Polygon3): Ray => {
  return interactPolys(ray, triangulate(polygon), triangleReflection)
}

export const roomIntersection = (ray: Ray, room: Room): Vector3 => {
  return interactPolys(ray, room.surfaces, polygonIntersection)
}
export const roomReflection = (ray: Ray, room: Room): Ray => {
  return interactPolys(ray, room.surfaces, polygonReflection)
}

