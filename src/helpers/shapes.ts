import { Vector3, Mesh } from '@models'
import { vectorize } from '@helpers/math'
import { ORIGIN } from '@constants'

export const rect3D = (
  length: number, width: number, height: number, center: Vector3 = ORIGIN
): Mesh => {
  const x0: number = center.x.valueOf() - length / 2
  const y0: number = center.y.valueOf() - width / 2
  const z0: number = center.z.valueOf() - height / 2
  const xm: number = center.x.valueOf() + length / 2
  const ym: number = center.y.valueOf() + width / 2
  const zm: number = center.z.valueOf() + height / 2
  /*
                  y

                  |
                  |

   (4)+ + + + + + + + + + +(3)
    +\                      +\
    + \                     + \
    +  \                    +  \
    +   \                   +   \
    +   (8)+ + + + + + + + +++ +(7)
    +    +                  +    +
    +    +                  +    +
    +    +                  +    +
    +    +     [ORIGIN]     +    +  ---- x
    +    +                  +    +
    +    +                  +    +
   (1)+ +++ + + + + + + + +(2)   +
     \   +                   \   +
      \  +                    \  +
       \ +                     \ +
        \+                      \+
        (5)+ + + + + + + + + + +(6)

                        \
                         \

                           z
   */
  // points
  const p1: Vector3 = vectorize(x0, y0, z0)
  const p2: Vector3 = vectorize(xm, y0, z0)
  const p3: Vector3 = vectorize(xm, ym, z0)
  const p4: Vector3 = vectorize(x0, ym, z0)
  const p5: Vector3 = vectorize(x0, y0, zm)
  const p6: Vector3 = vectorize(xm, y0, zm)
  const p7: Vector3 = vectorize(xm, ym, zm)
  const p8: Vector3 = vectorize(x0, ym, zm)
  return { surfaces: [
    { vertices: [p1, p2, p3, p4] }, // back
    { vertices: [p5, p8, p7, p6] }, // front
    { vertices: [p1, p5, p6, p2] }, // bottom
    { vertices: [p3, p7, p8, p4] }, // top
    { vertices: [p1, p4, p8, p5] }, // left
    { vertices: [p2, p6, p7, p3] }, // right
  ]}
}

export const cube = (side: number, center: Vector3 = ORIGIN): Mesh => {
  return rect3D(side, side, side, center)
}

