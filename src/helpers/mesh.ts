import { Vector3, Edge, Mesh } from '@models'

// get array of unique 3D points defining a mesh
export const uniquePoints = (mesh: Mesh): Vector3[] => {
  return [...new Set(mesh.surfaces.flatMap(s => s.vertices))]
}

// get array of unique edges defining a mesh
export const uniqueEdges = (mesh: Mesh): Edge[] => {
  // assume vertices of adjacent indices within a surface are linked
  // map vertex pairs like: [[vertex, vertex_next]...]
  // save only unique pairs
  const edgeMap = new Map<Vector3, Map<Vector3, boolean>>()
  mesh.surfaces.forEach(s => {
    s.vertices.map((v, i) => {
      const vn = s.vertices[(i + 1) % s.vertices.length]
      // ensure the biggest [x,y,z] coordinate is at index 0
      return (Object.values(v) > Object.values(vn)) ? [v, vn] : [vn, v]
    }).forEach(e => {
      edgeMap.has(e[0]) ?
        edgeMap.get(e[0]).set(e[1], true) :
        edgeMap.set(e[0], new Map([[e[1], true]]))
    })
  })
  // retrieve mapped data as unique edge array with two vertices per edge
  const edges: Edge[] = []
  for (const e of edgeMap.entries()) {
    for (const k of e[1].keys()) {
      edges.push({ vertices: [e[0], k] })
    }
  }
  return edges
}

// invoke the Euler-Poincaré formula to discern some basic qualities of a mesh
export const eulerPoincare = (mesh: Mesh): number => {
  // V = # vertices, F = # faces, E = # edges
  //
  // V + F - E = x
  //
  // x === 2 ? conventional enclosed space
  // subtract 2 from x for every genus present in the mesh (toroidal shapes)
  // x === 0 ? torus ; x === -2 ? double torus ; etc
  const f = mesh.surfaces.length
  const v = uniquePoints(mesh).length
  const e = uniqueEdges(mesh).length
  return v + f - e
}
