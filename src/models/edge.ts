import { Vector } from './vector'

export interface Edge {
  vertices: Vector[]
}
