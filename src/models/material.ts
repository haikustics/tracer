import Fraction from 'fraction.js'
import { MaterialType } from '@enums'

export interface Material {
  name: string
  materialType: MaterialType
  absorptionCoefficients: {
    31: Fraction
    63: Fraction
    125: Fraction
    250: Fraction
    500: Fraction
    1000: Fraction
    2000: Fraction
    4000: Fraction
    8000: Fraction
    16000: Fraction
  }
  nrc: Fraction
  scatteringCoefficient: Fraction
}

