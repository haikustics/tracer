import { Vector3 } from './vector'

export interface Ray {
  origin: Vector3
  direction: Vector3
}

