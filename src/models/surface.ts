import { Polygon3 } from './polygon'

export interface Surface extends Polygon3 {
  material?: number
}

