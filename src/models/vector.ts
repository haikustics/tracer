import Fraction from 'fraction.js'

export interface Vector {
  x: Fraction
  y: Fraction
  z?: Fraction
}

export type Vector3 = Required<Vector>

export type Vector2 = Omit<Vector, 'z'>
