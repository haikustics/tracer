import { Vector3 } from './vector'
import { ArrayOneOrMore } from './common'

export interface Plane {
  normal: Vector3
  vertices: ArrayOneOrMore<Vector3>
}

