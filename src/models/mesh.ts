import { Polygon3 } from './polygon'

export interface Mesh {
  surfaces: Polygon3[]
}

