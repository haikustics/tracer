import { Vector3 } from './vector'

export interface Source {
  point: Vector3
  directions: Vector3[]
}

