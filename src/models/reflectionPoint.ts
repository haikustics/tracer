import { Vector3 } from './vector'

export interface ReflectionPoint {
  normal: Vector3
  point: Vector3
}

