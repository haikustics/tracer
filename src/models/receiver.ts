import { Vector3 } from './vector'

export interface Receiver {
  point: Vector3
}

