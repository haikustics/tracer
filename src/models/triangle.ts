import { Polygon2, Polygon3 } from './polygon'
import { Vector2, Vector3 } from './vector'

export type Triangle = Triangle2 | Triangle3

export interface Triangle2 extends Polygon2 {
  vertices: [Vector2, Vector2, Vector2]
}

export interface Triangle3 extends Polygon3 {
  vertices: [Vector3, Vector3, Vector3]
}

