/* eslint-disable */
// TODO: remove eslint ignore when ts is better supported
import { Triangle2, Triangle3 } from './triangle'
import { Polygon2, Polygon3 } from './polygon'
import { Vector2, Vector3 } from './vector'

export type Type2D<T> =
  T extends Triangle3 ? Triangle2 :
  T extends Polygon3 ? Polygon2 :
  T extends Vector3 ? Vector2 :
    undefined

export type Type3D<T> =
  T extends Triangle2 ? Triangle3 :
  T extends Polygon2 ? Polygon3 :
  T extends Vector2 ? Vector3 :
    undefined

