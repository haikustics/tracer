export interface ArrayOneOrMore<T> extends Array<T> {
  0: T
}

export interface ArrayThreeOrMore<T> extends Array<T> {
  0: T
  1: T
  2: T
}

