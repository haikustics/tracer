import { Mesh } from './mesh'
import { Surface } from './surface'

export interface Room extends Mesh {
  surfaces: Surface[]
}

