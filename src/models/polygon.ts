import { ArrayThreeOrMore } from './common'
import { Vector2, Vector3 } from './vector'

export type Polygon = Polygon2 | Polygon3

export interface Polygon3 {
  vertices: ArrayThreeOrMore<Vector3>
}

export interface Polygon2 {
  vertices: ArrayThreeOrMore<Vector2>
}

