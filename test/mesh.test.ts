import { Vector3, Polygon3, Mesh } from '@models'
import { MeshType } from '@enums'
import { cube, rect3D } from '@helpers/shapes'
import { uniquePoints, uniqueEdges, eulerPoincare } from '@helpers/mesh'
import { vectorize } from '@helpers/math'

// points
// cube
const p1: Vector3 = vectorize(-1, -1, -1)
const p2: Vector3 = vectorize( 1, -1, -1)
const p3: Vector3 = vectorize( 1,  1, -1)
const p4: Vector3 = vectorize(-1,  1, -1)
const p5: Vector3 = vectorize(-1, -1,  1)
const p6: Vector3 = vectorize( 1, -1,  1)
const p7: Vector3 = vectorize( 1,  1,  1)
const p8: Vector3 = vectorize(-1,  1,  1)

// notch
const p9: Vector3 =  vectorize(-1,  0, 0.5)
const p10: Vector3 = vectorize( 1,  0, 0.5)

// surfaces
// bottom
const cs1: Polygon3 = { vertices: [p1, p2, p3, p4] }
// front
const cs3: Polygon3 = { vertices: [p1, p5, p6, p2] }
// back
const cs4: Polygon3 = { vertices: [p4, p3, p7, p8] }

// cube top
// const cs2: Polygon3 = { vertices: [p5, p8, p7, p6] }
// cube sides
// const cs5: Polygon3 = { vertices: [p1, p4, p8, p5] }
// const cs6: Polygon3 = { vertices: [p2, p6, p7, p3] }

// equivalent cube shape
const c: Mesh = cube(2)

// concave top
const cs7: Polygon3 = { vertices: [p5, p9, p10, p6] }
const cs8: Polygon3 = { vertices: [p7, p10, p9, p8] }
// concave sides
const cs9: Polygon3 = { vertices: [p1, p4, p8, p9, p5] }
const cs10: Polygon3 = { vertices: [p2, p6, p10, p7, p3] }


describe('Mesh', () => {
  describe('given six surfaces of a rectangular prism', () => {
    const rect: Mesh = rect3D(2, 3, 4)
    test('has eight unique points', () => {
      expect(
        uniquePoints(rect)
      ).toHaveLength(8)
    })
    test('has twelve unique edges', () => {
      expect(
        uniqueEdges(rect)
      ).toHaveLength(12)
    })
    test('has an Euler-Poincaré value of 2', () => {
      expect(
        eulerPoincare(rect)
      ).toBe(MeshType.Enclosed)
    })
  })
  describe('given six surfaces of a cube', () => {
    test('has eight unique points', () => {
      expect(
        uniquePoints(c)
      ).toHaveLength(8)
    })
    test('has twelve unique edges', () => {
      expect(
        uniqueEdges(c)
      ).toHaveLength(12)
    })
    test('has an Euler-Poincaré value of 2', () => {
      expect(
        eulerPoincare(c)
      ).toBe(MeshType.Enclosed)
    })
  })
  describe('given five surfaces of a cube (open box)', () => {
    const openBox: Mesh = {
      surfaces: c.surfaces.slice(0, 5)
    }
    test('has eight unique points', () => {
      expect(
        uniquePoints(openBox)
      ).toHaveLength(8)
    })
    test('has twelve unique edges', () => {
      expect(
        uniqueEdges(openBox)
      ).toHaveLength(12)
    })
    test('has an Euler-Poincaré value of 1', () => {
      expect(
        eulerPoincare(openBox)
      ).toBe(MeshType.Open)
    })
  })
  describe('given four surfaces of a cube (throne)', () => {
    const throne: Mesh = {
      surfaces: [c.surfaces[0]].concat(c.surfaces.slice(3))
    }
    test('has eight unique points', () => {
      expect(
        uniquePoints(throne)
      ).toHaveLength(8)
    })
    test('has eleven unique edges', () => {
      expect(
        uniqueEdges(throne)
      ).toHaveLength(11)
    })
    test('has an Euler-Poincaré value of 1', () => {
      expect(
        eulerPoincare(throne)
      ).toBe(MeshType.Open)
    })
  })
  describe('given four surfaces of a cube (hoop / torus)', () => {
    const torus: Mesh = {
      surfaces: c.surfaces.slice(0, 4)
    }
    test('has eight unique points', () => {
      expect(
        uniquePoints(torus)
      ).toHaveLength(8)
    })
    test('has twelve unique edges', () => {
      expect(
        uniqueEdges(torus)
      ).toHaveLength(12)
    })
    test('has an Euler-Poincaré value of 0', () => {
      expect(
        eulerPoincare(torus)
      ).toBe(MeshType.Torus)
    })
  })
  describe('given seven surfaces of a cubic volume with a single concave notch', () => {
    const divot: Mesh = {
      surfaces: [cs1, cs3, cs4, cs7, cs8, cs9, cs10]
    }
    test('has ten unique points', () => {
      expect(
        uniquePoints(divot)
      ).toHaveLength(10)
    })
    test('has fifteen unique edges', () => {
      expect(
        uniqueEdges(divot)
      ).toHaveLength(15)
    })
    test('has an Euler-Poincaré value of 2', () => {
      expect(
        eulerPoincare(divot)
      ).toBe(MeshType.Enclosed)
    })
  })
})

