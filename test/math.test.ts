import { Vector2, Vector3 } from '@models'
import {
  unitVector,
  vectorize,
  dot,
  cross,
  add,
  subtract,
  divide,
  multiply,
  reflect,
  negate,
} from '@helpers/math'
import Fraction from 'fraction.js';

describe('Math', () => {
  describe('given one 3D vector', () => {
    const v: Vector2 = vectorize(1, 2, 3)
    test('can calculate a unit vector', () => {
      expect(
        unitVector(v)
      ).toEqual({
        x: new Fraction(3229204, 12082575),
        y: new Fraction(6458408, 12082575),
        z: new Fraction(3229204, 4027525)
      })
    }),
    test('can negate', () => {
      expect(
        negate(v)
      ).toEqual(vectorize(-1,-2,-3))
    })
  })
  describe('given one zero-length vector', () => {
    const v = vectorize(0, 0, 0)
    test('will no-op on a unit vector', () => {
      expect(
        unitVector(v)
      ).toEqual(v)
    })
  })
  describe('given two 3D vectors', () => {
    const v1: Vector3 = vectorize(1, 2, 3)
    const v2: Vector3 = vectorize(3, 2, 1)

    test('can calculate a dot product', () => {
      expect(
        dot(v1, v2)
      ).toEqual(
        new Fraction(10)
      )
    })
    test('can calculate a cross product', () => {
      expect(
        cross(v1, v2)
      ).toEqual(
        vectorize(-4, 8, -4)
      )
    })
    test('can add vectors by component', () => {
      expect(
        add(v1, v2)
      ).toEqual(
        vectorize(4, 4, 4)
      )
    })
    test('can subtract vectors by component', () => {
      expect(
        subtract(v1, v2)
      ).toEqual(
        vectorize(-2, 0, 2)
      )
    })
    test('can reflect first vector over the second', () => {
      expect(
        reflect(v1, unitVector(v2))
      ).toEqual({
        x: new Fraction(31978459319089, 9732574575375).mul(-1),
        y: new Fraction(25026620336678, 29197723726125).mul(-1),
        z: new Fraction(45882137283911, 29197723726125)
      })
    })
  })
  describe('given a 3D vector and a scalar', () => {
    const v: Vector3 = vectorize(3, 4, 1)
    const s = new Fraction(2)
    test('can multiply vector components by scalar', () => {
      expect(
        multiply(v, s)
      ).toEqual(
        vectorize(6, 8, 2)
      )
    })
    test('can divide vector components by scalar', () => {
      expect(
        divide(v, s)
      ).toEqual(
        vectorize(1.5, 2, 0.5)
      )
    })
  })
})
