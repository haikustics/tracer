import { Vector3, Polygon3, Ray } from '@models'
import { vectorize, reflect } from '@helpers/math'
import { cube } from '@helpers/shapes'
import { planeNormal } from '@helpers/geometry'
import { roomIntersection, roomReflection } from '@helpers/ray'
import { ORIGIN } from '@constants'

describe('Ray', () => {
  describe('given a ray and enclosing room', () => {
    // ray from [0, 0] in arbitrary direction
    const ray: Ray = {
      origin: ORIGIN,
      direction: vectorize(0.5, 0.4, 0.3)
    }
    // 2x2 cube with points centered on [0, 0, 0]
    // -1 <= x, y, z <= 1
    const room = cube(2)
    // also given a pre-calculated intersection on the plane through x === 1
    const intersection: Vector3 = vectorize(1, 0.8, 0.6)
    // ...that occurs within the "right" surface
    const right: Polygon3 = room.surfaces[5]
    // and a pre-calculated reflection vector
    const reflection: Vector3 = vectorize(-0.5, 0.4, 0.3)
    test('can compute an intersection', () => {
      expect(
        roomIntersection(ray, room)
      ).toEqual(intersection)
    })
    test('reflects over a normal', () => {
      expect(
        reflect(
          ray.direction,
          planeNormal(right)
        )
      ).toEqual(reflection)
    })
    test('can compute a first order reflection intersection', () => {
      const ray2: Ray = {
        origin: intersection,
        direction: reflection
      }
      expect(
        roomReflection(ray2, room)
      ).toEqual({
        direction: vectorize(-0.5, -0.4, 0.3),
        origin: vectorize(0.75, 1, 0.75)
      })
    })
  })
  describe('given a ray and non-intersecting room', () => {
    // ray from [0, 0] in negative z direction
    const ray: Ray = {
      origin: ORIGIN,
      direction: vectorize(0, 0, -1)
    }
    // 2x2 cube with points centered on [3, 3, 3]
    // 2 <= x, y, z <= 4
    const room = cube(2, vectorize(3, 3, 3))
    test('cannot compute an intersection', () => {
      expect(
        roomIntersection(ray, room)
      ).toBeUndefined()
    })
  })
})
