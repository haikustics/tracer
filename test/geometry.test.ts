import { Triangle3, Polygon3 } from '@models'
import {
  planeNormal,
  convert3Dto2D,
  triangulate,
  testCoplanar,
  fibonacciSphere
} from '@helpers/geometry'
import { vectorize } from '@helpers/math'

import Fraction from 'fraction.js'

describe('Geometry', () => {
  describe('given a 3D triangle', () => {
    const t1: Triangle3 = {
      vertices: [
        vectorize(0, 0, 1),
        vectorize(3, 1, 2),
        vectorize(4, 4, 3)
      ]
    }
    test('can verify that points are coplanar', () => {
      expect(
        testCoplanar(t1.vertices)
      ).toEqual(true)
    })
    test('can accurately calculate a normal', () => {
      expect(
        planeNormal(t1)
      ).toEqual(
        vectorize(-0.23570226039551587, -0.23570226039551587, 0.9428090415820635)
      )
    })
    test('can be transposed to 2D', () => {
      expect(
        convert3Dto2D(t1)
      ).toEqual(
        { vertices: [
          vectorize(0, 0),
          {
            x: new Fraction(8438083006757144, 2733447560951177),
            y: new Fraction(7771476155864919, 6408523411521377)
          },
          {
            x: new Fraction(33752332027028572, 8200342682853531),
            y: new Fraction(27977314161113708, 6408523411521377)
          }
        ]}
      )
    })
  })
  describe('given a polygon as a five point array with a single reflex vertex', () => {
    const p2: Polygon3 = {
      vertices: [
        vectorize(0, 0, 0),
        vectorize(0, 4, 0),
        vectorize(4, 4, 0),
        vectorize(5, 5, 0),
        vectorize(4, 0, 0)
      ]
    }
    test('can verify that points are coplanar', () => {
      expect(
        testCoplanar(p2.vertices)
      ).toEqual(true)
    })
    test('can accurately calculate a normal', () => {
      expect(
        planeNormal(p2)
      ).toEqual(
        vectorize(0, 0, -1)
      )
    })
    test('can be triangulated into three triangles', () => {
      expect(
        triangulate(p2)
      ).toHaveLength(3)
    })
  })
  describe('given a polygon as a four point array with a single nonplanar vertex', () => {
    const p3: Polygon3 = {
      vertices: [
        vectorize(0, 0, 0),
        vectorize(0, 4, 0),
        vectorize(4, 4, 0),
        vectorize(5, 5, 1)
      ]
    }
    test('can verify that points are not coplanar', () => {
      expect(
        testCoplanar(p3.vertices)
      ).toEqual(false)
    })
  })
  describe('given a sample size', () => {
    const sample = 1000
    test('can generate a random fibonacci unit sphere', () => {
      expect(
        fibonacciSphere(sample)
      ).toHaveLength(1000)
    })
    test('can generate a non-random fibonacci unit sphere', () => {
      expect(
        fibonacciSphere(sample, false)
      ).toHaveLength(1000)
    })
  })
})
