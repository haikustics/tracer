import { ORIGIN } from '@constants'
import { Ray, Room, Source } from '@models'
import { cube } from '@helpers/shapes'
import { reflectOrder, reflectSource } from '@helpers/acoustics'
import { fibonacciSphere } from '@helpers/geometry'
import { vectorize } from '@helpers/math'

describe('Acoustics', () => {
  // 2x2 cube with points centered on [0, 0]
  // -1 <= x, y, z <= 1
  const room: Room = cube(2)
  describe('given a ray and enclosing room', () => {
    // ray from [0, 0] in arbitrary direction
    const ray: Ray = {
      origin: ORIGIN,
      direction: vectorize(0.5, 0.4, 0.3)
    }
    test('can calculate a first order reflection', () => {
      expect(
        reflectOrder(ray, room)
      ).toEqual({
        origin: vectorize(1, 0.8, 0.6),
        direction: vectorize(-0.5, 0.4, 0.3)
      })
    })
    test('can calculate a second order reflection', () => {
      expect(
        reflectOrder(ray, room, 2)
      ).toEqual({
        origin: vectorize(0.75, 1.0, 0.75),
        direction: vectorize(-0.5, -0.4, 0.3)
      })
    })
    test('can calculate a third order reflection', () => {
      expect(
        reflectOrder(ray, room, 3)
      ).toEqual({
        origin: vectorize(1/3, 2/3, 1),
        direction: vectorize(-0.5, -0.4, -0.3)
      })
    })
  })
  describe('given a source and enclosing room', () => {
    const source: Source = {
      point: ORIGIN,
      directions: fibonacciSphere(1000)
    }
    test('can calculate all first order reflections', () => {
      expect(
        reflectSource(source, room)
      ).not.toContain(undefined)
    })
    test('can calculate all second order reflections', () => {
      expect(
        reflectSource(source, room, 2)
      ).not.toContain(undefined)
    })
    test('can calculate all third order reflections', () => {
      expect(
        reflectSource(source, room, 3)
      ).not.toContain(undefined)
    })
  })
})
