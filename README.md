# Haikustics Acoustic Ray Tracer

Software package/library for acoustical modeling of 3D spaces.

## Developers

`npm i` to install packages

`npm run lint` to run eslint

`npm test` to run jest tests

`npm run build` to build code with tsc

`npm start` to build code with tsc and watch for changes

`npm run cli` to open a ts-node cli that respects tsconfig compilerOptions paths

